---
layout: extended.njk
---

<div class="container intro">
  <h1 class="motto">Let’s build software that <b> lasts. </b></h1>
  <img src="/static/beyond-expiry-wordmark.svg" alt="open can with infinity symbol on it" />
</div>
<div class="end-panel">
  <section class="container">
    <h2>Projects</h2>
    <ul>
      <li>
        <b><a href="https://fuiz.us">Fuiz.</a></b> In-classroom quizzing tool
        similar to Kahoot.
      </li>
      <li>
        <b><a href="https://flathub.org/apps/io.gitlab.adhami3310.Converter">Switcheroo.</a></b>
        Image format conversion tool.
      </li>
      <li>
        <b><a href="https://flathub.org/apps/io.gitlab.adhami3310.Impression">Impression.</a></b>
        Disk image burner.
      </li>
      <li>
        <b><a href="https://flathub.org/apps/io.gitlab.adhami3310.Footage">Footage.</a></b>
        Simple video editor.
      </li>
    </ul>
  </section>
  <section class="container">
    <h2>Team</h2>
    <ul>
      <li>
        <em><a href="https://www.linkedin.com/in/yichen-gao/">Yichen Gao.</a></em>
        Non-profit president.
      </li>
      <li>
        <em><a href="https://www.linkedin.com/in/gabriel-rodriguez-castillo-20336022a/">Gabriel Rodriguez
            Castillo.</a></em>
        Non-profit treasurer.
      </li>
      <li>
        <em><a href="https://adhami.me/">Khaleel Al-Adhami.</a></em> Lead
        developer and non-profit clerk.
      </li>
      <li>
        <em><a href="https://barish.me/about/">Abutalib Namazov.</a></em>
        Developer.
      </li>
    </ul>
  </section>
</div>
<section class="container">
  <h2>Software, Like Food, Expires</h2>
  <p>
    The fact of the matter is, most software ever created is no longer usable.
    Many are lost to time and others have yet to be updated to be functional on
    modern hardware. Worse yet, some are not being sold anymore and also
    prevented from entering the public domain.
  </p>
  <p>
    A very good example of this matter is how Google killed approximately 300 of
    their products. The term
    <a href="https://killedbygoogle.com/">Google graveyard</a> has been coined
    to describe this issue. The problem lies here that Google maintenance was
    necessary to keep these services open, and once that has been taken away,
    the services have been taken down.
  </p>
  <p>
    Another example is Nintendo repeatedly pulling the lawsuit card against
    various open source projects that emulate their old hardware. The irony here
    is that Nintendo doesn't sell most of these games anymore; meaning that they
    are killing off the only way to play these games.
  </p>
</section>
<section class="container">
  <h2>This is bad</h2>
  <p>
    It really is. The amount of human hours wasted on software that expired is
    extremely large. If we were to develop our apps in ways that prevent this
    from happening we would be much more productive in developing time and make
    progress quickly. Do you want to study how to make a compiler for an old CPU
    architecture? There is a bunch of software to study from.
  </p>
  <p>Imagine hiring a carpenter with the following premise:</p>
  <blockquote>
    If I go out of business, all your carpet will evaporate.
  </blockquote>
  <p>
    Will you buy from this carpenter? I don't think so. But that carpenter is
    Figma, and it's the biggest software mockup design tool. For technology, we
    seem determined that such a deal is not just acceptable, but encouraged.
  </p>
</section>
<section class="container">
  <h2>Beyond Expiry Principles</h2>
  <p>To address these problems, we advise the following principles:</p>
  <ol>
    <li>
      <b>Open Source, always.</b> The availability of the source code and the
      legality of modifying and studying it provides an inherent resistance
      towards expiry. However, it's not enough on its own.
    </li>
    <li>
      <b>Use modern tooling.</b> There are programming languages best suited
      towards reproducibility than others. Those are most ideal as one might not
      have the "compilation instructions" along with the source code. In
      addition, software and standards used must abide by the beyond expiry
      principles.
    </li>
    <li>
      <b>Avoid vendor lock-in standards.</b> If there's two options: use an
      existing standard or create a new one, ninety-nine out of a hundred times
      one should attempt to use existing standards; as standards expire as well.
      It also ties user data to the software in question, and its eventual
      expiry. The best at achieving this was
      <a href="https://obsidian.md/">Obsidian</a>, as it built on top of
      existing markdown standards instead of building its own.
    </li>
    <li>
      <b>Avoid centralization.</b> While developing an open source Twitter-like
      platform can be cool on its own; it carries the clear problem of having
      one center of command. This makes it extremely vulnerable to expiry once
      the main maintainer steps down. Ideally, such software should be federated
      and decentralized to achieve resistance towards such issues.
    </li>
    <li>
      <b>Use fair payment models.</b> Software is no different than other
      products, scamming isn't allowed. If one is paying for the product one
      must pay a flat fee. If one is paying for maintenance then one pays a
      recurring fee. Ideally these two options should be offered and clearly
      conveyed.
    </li>
    <li>
      <b>Use user storage.</b> An issue in modern software is the over reliance
      of each app on storing user data on their premises. This is another cause
      for vendor lock-in and larger consequences if the software expires. To
      fight against this, there must be an option of storing user data in their
      personal choice of cloud storage if possible.
    </li>
    <li>
      <b>Simplify your stack.</b> Avoid overcomplicating your stack in ways that
      would prevent developers from studying your software effectively.
    </li>
  </ol>
</section>
<section class="container">
  <h2>Beyond Expiry Applied; VSCode vs Repl.it</h2>
  <p>
    Let's apply these principles to separeate VSCode from one of its wild
    competitors, the online IDE Repl.it.
  </p>
  <p>
    VSCode is (mostly) open source. It's written in a language with modern
    tooling and it's reasonably easy to tweak (check VSCodium for proof). VSCode
    introduces very little standards (besides VSIX). It's also possible to
    install extensions through local files to avoid going through the Microsoft
    provided repo. It also priotirizes the user's storage (even though I have
    seen some push towards online VSCode recently). So it checks all of our
    boxes for beyond expiry (except possibly a fair payment model, because it's
    funded entirely through Microsoft). We would expect VSCode to survive if it
    was to be abandoned by Microsoft.
  </p>
  <p>
    Repl.it on the other almost doesn't hit any aspect. Due to the nature of its
    closed source stack and its reliance on centralization (the website), and
    its abundant use of cloud storage raises the question of "if Repl.it was to
    close tomorrow, what are you going to do?". The answer to that question is
    probably to move to VSCode.
  </p>
  <p>
    The comparison here isn't simply just about the features, but the inherent
    design choices made by both teams. If Repl.it had beyond expiry principles
    in mind, they would have designed something very similar to VSCode. This is
    not to mean they did something wrong per se, but to illustrate that their
    sales pitch is inherently risky to anyone wanting a product that will live
    on.
  </p>
</section>