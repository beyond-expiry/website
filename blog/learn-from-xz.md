---
title: XZ attack will happen again
desc: because we never learn
layout: blog.njk
date: git Created
tags: post
---

## Trust

> "Trust is hard-earned, easily lost, and difficult to reestablish." ~ [Carol L. Folt](https://we-are.usc.edu/2021/11/08/11-8-21-message-from-president-folt/)

I disagree with the statement above. I think trust must never be there. Ideally you must not trust anything, not even yourself.

However, we have to make compromises. I'm willing to trust something if I have some kind of *insurance*. This ensurance can be of many forms, but to me, *the most effective kind of insurance is libaility.* If the person would be liable for misconduct, then I can establish some degree of trust.

In the XZ attack, an anonymous programmer was trusted, when they cannot be held liable for their damages. We will continue to trust random personas on the internet.

I maintain multiple pieces of software that is used by [hundreds of thousands of people](https://flathub.org/apps/io.gitlab.adhami3310.Impression). I don't trust myself to not do something that will break their devices, which is why I use Flatpak and its native sandboxing to limit any kind of failures on my side to destroy the host machine. It also helps to monitor contributions to the program as any contribution that changes the sandboxing permission will sound multiple alarms.

## Security

Your system is as secure as the least secure aspect of it. In this case, SSHD was as secure as as XZ. We will continue to assume dependancies are secure.

We had many opportunities to learn this. The first is when `left-pad` got yanked off of the npm registery causing numerous build failures. The other opportunity was `node-ipc` where a programmer inserted code to wipe off files of Russian users. The third time is now, and we won't learn.

## Funding

XZ was a hobby project, but a very crucial one. In any world that makes sense, the maintainer would have been paid generously to maintain a crucial part of the internet. We will continue to leave the open source world wholly unfunded.